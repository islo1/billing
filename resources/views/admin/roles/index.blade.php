@extends('layouts.app')


@section('content')
<div class="card p-0">
    <div class="card-body">
        <h5 class="card-title vh-25">
            <div class="float-start fs-4">
                Role Management
            </div>
            <div class="float-end">
                <a class="btn btn-success" href="{{ route('roles.create') }}"> Create</a>
            </div>
        </h5>
    </div>
    <div class="card-body">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <!-- Table with stripped rows -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-2">No</th>
                    <th class="col-md-4">Name</th>
                    <th class="col-md-3">Editor</th>
                    <th class="col-md-3">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $key => $role)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->editor }}</td>
                    <td>
                        <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}"><i class="bi bi-eye h5"></i></a>
                        @can('role-edit')
                            <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}"><i class="bi bi-pencil-square h5"></i></a>
                        @endcan
                        @can('role-delete')
                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                {!! Form::button('<i class="bi bi-trash3 h5"></i>', ['class' => 'btn btn-danger show-alert-delete-box']) !!}
                            {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- End Table with stripped rows -->
    </div>
</div>

{!! $roles->render() !!}

@endsection
@extends('layouts.app')


@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Edit New User</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-secondary" href="{{ route('users.index') }}"> Back</a>
            </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-xl-9">
            <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
                        <div class="row align-items-center pt-4 pb-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Name</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control form-control-lg')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-2">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control form-control-lg')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-2">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Password</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control form-control-lg')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-2">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Confirm Password</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control form-control-lg')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-2">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Role</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control form-control-lg','multiple')) !!}
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">State</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                {!! Form::radio('state', '0',null,['required' => 'true','class' => 'form-check-input']) !!}
                                {!! Form::label('legal', 'Disable', ['class' => 'form-check-label']) !!}
                                {!! Form::radio('state', '1',null,['required' => 'true','class' => 'form-check-input']) !!}
                                {!! Form::label('legal', 'Enable', ['class' => 'form-check-label']) !!}
                            </div>
                        </div>

                        <div class="px-5 py-4 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
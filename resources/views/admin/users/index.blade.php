@extends('layouts.app')


@section('content')
<div class="card p-0">
    <div class="card-body">
        <h5 class="card-title vh-25">
            <div class="float-start fs-4">
                Users Management
            </div>
            <div class="float-end">
                <a class="btn btn-success" href="{{ route('users.create') }}"> Create</a>
            </div>
        </h5>
    </div>
    <div class="card-body">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <!-- Table with stripped rows -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-1">No</th>
                    <th class="col-md-2">Name</th>
                    <th class="col-md-3">Email</th>
                    <th class="col-md-2">Roles</th>
                    <th class="col-md-2">State</th>
                    <th class="col-md-2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $v)
                        <label class="badge bg-info">{{ $v }}</label>
                        @endforeach
                        @endif
                    </td>
                    <td>
                        @if(!empty($user->state))
                        <span class="badge text-bg-success fs-6">Enable</span>
                        @else
                        <span class="badge text-bg-secondary fs-6">Disable</span>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-info" href="{{ route('users.show',$user->id) }}"><i class="bi bi-eye h5"></i></a>
                        @can('user-edit')
                        <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}"><i class="bi bi-pencil-square h5"></i></a>
                        @endcan
                        
                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                        @can('user-delete')
                        {!! Form::button('<i class="bi bi-trash3 h5"></i>', ['class' => 'btn btn-danger show-alert-delete-box']) !!}
                        @endcan
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- End Table with stripped rows -->
    </div>
</div>

<script>
    $( function() {
        $('.table').DataTable(opt);
    });
</script>
@endsection
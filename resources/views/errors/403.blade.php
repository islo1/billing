@extends('layouts.app')

@section('title', '403 Forbidden')

@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>403 Forbidden</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-secondary" href="{{ back()->getTargetUrl() }}"> Back</a>
            </div>
        </div>
        <div class="col-xl-9">
            <div class="card">
                <div class="card-body p-5">
                    <div class="float-start mx-3"><i class="bi bi-bug display-3"></i></div>
                    <div class="float-center fs-3">您沒有權限訪問該頁面。</div>
                    <div class="float-center fs-4">如果您認為這是一個錯誤，請聯繫網站管理員。</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

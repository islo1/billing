@extends('layouts.app')

@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-xl-9">
            <div class="card" style="border-radius: 15px;">
                <div class="card-header fs-3">{{ __('Register') }}</div>
                <div class="card-body">
                    {!! Form::open(array('route' => 'register','method'=>'POST')) !!}
                        <div class="row align-items-center pt-2 pb-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">{{ __('Name') }}</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                <input id="name" type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required placeholder='Name' autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">{{ __('Email Address') }}</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder='Email' autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Password</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required placeholder='Password' autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="row align-items-center py-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">Confirm Password</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                <input id="password-confirm" type="password" class="form-control form-control-lg" name="password_confirmation" required placeholder='Confirm Password' autocomplete="new-password">
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="px-5 py-4 text-center">
                            <button type="submit" class="btn btn-primary">{{ __('Register') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

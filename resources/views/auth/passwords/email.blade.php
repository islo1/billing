@extends('layouts.app')

@section('content')
<div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>{{ __('Reset Password') }}</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-secondary" href="{{ back()->getTargetUrl() }}"> Back</a>
            </div>
        </div>
        <div class="col-xl-9">
            <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    {!! Form::open(array('route' => 'password.email','method'=>'POST')) !!}
                        <div class="row align-items-center pt-2 pb-3">
                            <div class="col-md-3 ps-5">
                                <h6 class="mb-0">{{ __('Email Address') }}</h6>
                            </div>
                            <div class="col-md-9 pe-5">
                                <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <hr class="mx-n3">

                        <div class="px-5 py-4 text-center">
                            <button type="submit" class="btn btn-primary">{{ __('Send Password Reset Link') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
